import React from 'react';
import Auth from '../modules/Auth';
import Products from '../components/Products.jsx';


class ProductsPage extends React.Component {

  /**
   * Class constructor.
   */
  constructor(props) {
    super(props);

      this.state = {
          products: [
          ]
      };
  }


    /**
     * This method will be executed after initial rendering.
     */
    componentDidMount() {
        const xhr = new XMLHttpRequest();
        let host = location.protocol + "//" + location.hostname + ":" + "3000/api/get_products";
        xhr.open('get', host);
        xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
        // set the authorization HTTP header
        xhr.setRequestHeader('Authorization', `bearer ${Auth.getToken()}`);
        xhr.responseType = 'json';
        xhr.addEventListener('load', () => {
            if (xhr.status === 200) {
                this.setState({
                    products: xhr.response.list
                });
            }
        });
        xhr.send();
    }

  /**
   * Render the component.
   */
  render() {
    return (<Products products={this.state.products} />);
  }

}

export default ProductsPage;
