import React from 'react';
import Auth from '../modules/Auth';
import Dashboard from '../components/Dashboard.jsx';


class DashboardPage extends React.Component {

  /**
   * Class constructor.
   */
  constructor(props) {
    super(props);

    this.state = {
      people: [],
        chartData: {
            labels: [],
            datasets: [
                {
                    label: "In",
                    fillColor: "rgba(220,220,220,0.2)",
                    strokeColor: "rgba(220,220,220,1)",
                    pointColor: "rgba(220,220,220,1)",
                    pointStrokeColor: "#fff",
                    pointHighlightFill: "#fff",
                    pointHighlightStroke: "rgba(220,220,220,1)",
                    data: []
                },
                {
                    label: "Out",
                    fillColor: "rgba(151,187,205,0.2)",
                    strokeColor: "rgba(151,187,205,1)",
                    pointColor: "rgba(151,187,205,1)",
                    pointStrokeColor: "#fff",
                    pointHighlightFill: "#fff",
                    pointHighlightStroke: "rgba(151,187,205,1)",
                    data: []
                }
            ]
        },
        chartOptions: {
          responsive: true
        }
    };

      this.handleData = this.handleData.bind(this);
      this.wsUrl = 'ws://' + location.hostname + ':1337';

      // chart
  }


    /**
     * This method will be executed after initial rendering.
     */
    componentDidMount() {
        let sideMenuToggle = $(".sideMenuToggle");

        sideMenuToggle.click(function (e) {
            e.preventDefault();
            let body = $("body");
            if (body.hasClass("mtSidebarOpen")) {
                let sidebarCollapsedLink = $(".mtSidebar a[data-toggle='collapse']:not(.collapsed)");
                sidebarCollapsedLink.addClass("collapsed");
                let sidebarCollapsedPanel = $(".mtSidebar .panel-collapse.in");
                sidebarCollapsedPanel.removeClass("in");
            }
            body.toggleClass("mtSidebarOpen");
        });
    }

    handleData(data) {
        let result = JSON.parse(data);
        let currentObject = this.state.chartData;
        if (result.first) {

            for (let i in result.List) {
                let currentMsg = result.List[i];

                // dates in labels
                currentObject.labels.splice(0, 0, this.dateTimeFormat(currentMsg.date));
                // currentObject.labels.splice(0, 0, 1);


                currentObject.datasets[0].data.splice(0, 0, currentMsg.in);
                currentObject.datasets[1].data.splice(0, 0, currentMsg.out);
            }
            let newList = result.List;
            for(let k in newList){
                newList[k].date = this.dateFormat(newList[k].date);
            }
            this.setState({people: result.List});
            this.setState({chartData: currentObject});
        }
        else {
            let list = this.state.people;
            let newData = {};
            newData.in = result.List.in;
            newData.out = result.List.out;
            newData.date = this.dateFormat(result.List.date);
            list.splice(0, 0, newData);
            if (list.length > 3) {
                list.pop();
            }

            // data chart part
            currentObject.labels.splice(0, 0, this.dateTimeFormat(newData.date));
            // currentObject.labels.splice(0, 0, 2);

            currentObject.datasets[0].data.splice(0, 0, newData.in);
            currentObject.datasets[1].data.splice(0, 0, newData.out);

            if (currentObject.labels.length > 3) {
                currentObject.labels.pop();
                currentObject.datasets[0].data.pop();
                currentObject.datasets[1].data.pop();
            }

            this.setState({people: list});
            this.setState({chartData: currentObject});
        }

    }

    dateTimeFormat(currentDate){

        return moment(currentDate).format("HH:mm:ss");
    }

    dateFormat(currentDate){

        return moment(currentDate).format("DD/MM/YYYY HH:mm:ss");
    }


  /**
   * Render the component.
   */
  render() {
    return (<Dashboard people={this.state.people} handleData={this.handleData} wsUrl={this.wsUrl} chartData={this.state.chartData} chartOptions={this.state.chartOptions} />);
  }

}

export default DashboardPage;
