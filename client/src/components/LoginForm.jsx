import React, { PropTypes } from 'react';
import { Link } from 'react-router';

const LoginForm = ({
  onSubmit,
  onChange,
  errors,
  successMessage,
  user
}) => (
   <div>
     <section className="loginSection">
       <form action="/" className="clearfix" onSubmit={onSubmit}>
         <section className="boxWrap">
           <div className="box">
             <div className="boxHeader">
               <div className="boxHeaderLeft">
                 <h3>
                   <i className="fa fa-user"></i>
                   <span>Login</span>
                 </h3>
               </div>
             </div>
             <div className="boxBody">
               <div className="row">
                 <div className="col-xs-12">
                   <div className="form-group">
                     <label htmlFor="email">Username:</label>
                     <input name="email" onChange={onChange} className="form-control" type="text" id="email" />
                   </div>
                   <div className="form-group">
                     <label htmlFor="password">Password:</label>
                     <input name="password" onChange={onChange} className="form-control" type="password" id="password" />
                   </div>
                 </div>
               </div>
               <div className="col-xs-12">
                   {successMessage && <p className="success-message">{successMessage}</p>}
                   {errors.summary && <p className="error-message">{errors.summary}</p>}
               </div>
             </div>
             <div className="boxFooter">
               <div className="boxFooterLeft">
                 {/*<div className="mtCheckbox">*/}
                   {/*<label>*/}
                     {/*<input type="checkbox" />*/}
                     {/*<span>Remember Me</span>*/}
                   {/*</label>*/}
                 {/*</div>*/}
               </div>
               <div className="boxFooterRight">
                 <button type="submit" className="btn btn-flat color-2-bg color-2-hover-bg color-text-white">Login</button>
               </div>
             </div>
           </div>
         </section>
       </form>
     </section>
   </div>
);

LoginForm.propTypes = {
  onSubmit: PropTypes.func.isRequired,
  onChange: PropTypes.func.isRequired,
  errors: PropTypes.object.isRequired,
  successMessage: PropTypes.string.isRequired,
  user: PropTypes.object.isRequired
};

export default LoginForm;
