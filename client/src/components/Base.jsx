import React, { PropTypes } from 'react';
import { Link, IndexLink } from 'react-router';
import Auth from '../modules/Auth';


const Base = ({ children }) => (
  <div>
    {/*<div className="top-bar">*/}
      {/*<div className="top-bar-left">*/}
        {/*<IndexLink to="/">React App</IndexLink>*/}
      {/*</div>*/}

      {/*{Auth.isUserAuthenticated() ? (*/}
        {/*<div className="top-bar-right">*/}
          {/*<Link to="/logout">Log out</Link>*/}
        {/*</div>*/}
      {/*) : (*/}
        {/*<div className="top-bar-right">*/}
          {/*<Link to="/login">Log in</Link>*/}
          {/*/!*<Link to="/signup">Sign up</Link>*!/*/}
        {/*</div>*/}
      {/*)}*/}

    {/*</div>*/}

      {Auth.isUserAuthenticated() ? (
        <div>
            <section id="wrap">
                <header className="color-1-bg">
                    <section className="headerLeft">
                        <div className="mtadminLogo-lg">
                            <a href="#"><img src="" alt="" /></a>
                        </div>
                        <div className="mtadminLogo-sm">
                            <a href="#"><img src="" alt="" /></a>
                        </div>
                    </section>
                    <section className="headerRight">
                        <div className="headerRight-Left">
                            <div>
                                <a href="#" className="sideMenuToggle color-1-hover-bg" title="Toggle Sidebar">
                                    <i className="fa fa-bars"></i>
                                </a>
                            </div>
                        </div>
                        <div className="headerRight-Right">
                            <div className="hdrUser dropdown">
                                <a href="#" className="hdrUserToggle color-1-hover-bg" title="User" data-target="#" id="hdrUSerToggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                    <img src="img/users/user-6.jpg" alt="" />
                                    <h2><span>User</span></h2>
                                </a>
                                <ul className="dropdown-menu dropdown-menu-right userDropdown">
                                    <li className="dropdown-menu-header color-1-bg">
                                        <img src="img/users/user-6.jpg" className="img-circle" alt=""/>
                                        <p>Beacon Web Interface</p>
                                    </li>
                                    <li className="dropdown-menu-footer userMenuFooter">
                                        <div className="pull-right">
                                            <Link to="/logout"><button type="button" className="btn btn-default">Sign out</button></Link>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            <div>
                                <a href="#" className="sideMenuToggle color-1-hover-bg" title="Settings">
                                    <i className="fa fa-wrench"></i>
                                </a>
                            </div>
                        </div>
                    </section>
                </header>
            </section>
            <section className="mtSidebar">
            <div className="sidebar-menu">
                <div className="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                    <div className="panel panel-default panel-bg">
                        <div className="panel-heading" role="tab">
                            <h4 className="panel-title">
                                <Link to="/">
                                    <div className="panel-title-left">
                                        <i className="fa fa-dashboard"></i>
                                        <span>Dashboard</span>
                                    </div>
                                </Link>
                            </h4>
                        </div>
                    </div>
                    <div className="panel panel-default panel-bg">
                        <div className="panel-heading" role="tab">
                            <h4 className="panel-title">

                                <Link to="/users">

                                    <div className="panel-title-left">
                                        <i className="fa fa-users"></i>
                                        <span>Users</span>
                                    </div>
                                </Link>
                            </h4>
                        </div>
                    </div>
                    <div className="panel panel-default panel-bg">
                        <div className="panel-heading" role="tab">
                            <h4 className="panel-title">
                                <Link to="/activity_log">
                                    <div className="panel-title-left">
                                        <i className="fa fa-th-list"></i>
                                        <span>Activity Log</span>
                                    </div>
                                </Link>
                            </h4>
                        </div>
                    </div>
                    <div className="panel panel-default panel-bg">
                        <div className="panel-heading" role="tab">
                            <h4 className="panel-title">
                                <Link to="/payments">
                                    <div className="panel-title-left">
                                        <i className="fa fa-usd"></i>
                                        <span>Payments</span>
                                    </div>
                                </Link>
                            </h4>
                        </div>
                    </div>
                    <div className="panel panel-default panel-bg">
                        <div className="panel-heading" role="tab">
                            <h4 className="panel-title">
                                <Link to="/products">
                                    <div className="panel-title-left">
                                        <i className="fa fa-shopping-bag"></i>
                                        <span>Products</span>
                                    </div>
                                </Link>
                            </h4>
                        </div>
                    </div>
                    {/*<div className="panel panel-default panel-bg">*/}
                        {/*<div className="panel-heading" role="tab">*/}
                            {/*<h4 className="panel-title">*/}
                                {/*<Link to="products.html">*/}
                                    {/*<div className="panel-title-left">*/}
                                        {/*<i className="fa fa-cog"></i>*/}
                                        {/*<span>Configuration</span>*/}
                                    {/*</div>*/}
                                {/*</Link>*/}
                            {/*</h4>*/}
                        {/*</div>*/}
                    {/*</div>*/}
                </div>
            </div>
        </section>
        </div>
          ) : (
              <div></div>
          )}

    { /* child component will be rendered here */ }
    {children}


      <footer>

      </footer>
      <script>

      </script>
  </div>
);

Base.propTypes = {
  children: PropTypes.object.isRequired
};

export default Base;
