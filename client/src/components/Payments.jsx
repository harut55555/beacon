import React, { PropTypes } from 'react';

const Payments = ({payments}) => (
  <div>
      <section className="contentWrapTop">
          <div className="row">
              <div className="col-xs-12">
                  <div className="contentWrapTopLeft">
                      <h1>Payments</h1>
                  </div>
              </div>
          </div>
      </section>
      <section className="contentWrap">
          <div className="row">
              <div className="col-xs-12">
                  <div className="typography text-right">
                      <h2 id="mtTime"></h2>
                  </div>
              </div>
          </div>
          <div className="row">
              <div className="col-xs-12">
                  <section className="boxWrap">
                      <div className="box">
                          <div className="boxHeader">
                              <div className="boxHeaderLeft">
                                  <h3>
                                      <i className="fa fa-usd"></i>
                                      <span>Payments</span>
                                  </h3>
                              </div>
                              <div className="boxHeaderRight">

                              </div>
                          </div>
                          <div className="boxBody">
                              <div className="tableWrap">
                                  <table className="table table-bordered">
                                      <thead>
                                      <tr>
                                          <th>#</th>
                                          <th>Amount</th>
                                          <th>Type</th>
                                          <th>Date</th>
                                          <th>Time</th>
                                      </tr>
                                      </thead>
                                      <tbody>

                                      {payments.map(function(object, i){
                                          return <tr  key={i}>
                                              {[
                                                  <td>{object.id}</td>,
                                                  <td>{object.amount}</td>,
                                                  <td>{object.type}</td>,
                                                  <td>{object.creation_date}</td>,
                                                  <td>{object.time}</td>
                                              ]}
                                          </tr>;
                                      })}

                                      </tbody>
                                  </table>
                              </div>
                          </div>
                      </div>
                  </section>
              </div>
          </div>
      </section>
  </div>
);

Payments.propTypes = {
    payments: PropTypes.array.isRequired
};


export default Payments;
