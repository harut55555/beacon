import React, { PropTypes } from 'react';

const ActivityLog = ({logs}) => (
    <div>
        <section className="contentWrapTop">
            <div className="row">
                <div className="col-xs-12">
                    <div className="contentWrapTopLeft">
                        <h1>Activity Log</h1>
                    </div>
                </div>
            </div>
        </section>
        <section className="contentWrap">
            <div className="row">
                <div className="col-xs-12">
                    <div className="typography text-right">
                        <h2 id="mtTime"></h2>
                    </div>
                </div>
            </div>
            <div className="row">
                <div className="col-xs-12">
                    <section className="boxWrap">
                        <div className="box">
                            <div className="boxHeader">
                                <div className="boxHeaderLeft">
                                    <h3>
                                        <i className="fa fa-th-list"></i>
                                        <span>Activity Log</span>
                                    </h3>
                                </div>
                                <div className="boxHeaderRight">

                                </div>
                            </div>
                            <div className="boxBody">
                                <div className="tableWrap">
                                    <table className="table table-bordered">
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>In</th>
                                            <th>Out</th>
                                            <th>Date</th>
                                        </tr>
                                        </thead>
                                        <tbody>

                                        {logs.map(function(object, i){
                                            return <tr  key={i}>
                                                {[
                                                    <td>{object.id}</td>,
                                                    <td>{object.in}</td>,
                                                    <td>{object.out}</td>,
                                                    <td>{object.creation_date}</td>
                                                ]}
                                            </tr>;
                                        })}

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </section>
    </div>
);

ActivityLog.propTypes = {
    logs: PropTypes.array.isRequired
};


export default ActivityLog;
