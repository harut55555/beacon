import React, { PropTypes } from 'react';
import { Link, IndexLink } from 'react-router';
import Websocket from 'react-websocket';
const BarChart = require("react-chartjs").Bar;

const Dashboard = ({ people,handleData,wsUrl,chartData,chartOptions }) => (
    <div>
       <section className="contentWrapTop">
            <div className="row">
                <div className="col-xs-12">
                    <div className="contentWrapTopLeft">
                        <h1>Dashboard</h1>
                    </div>
                    <div className="contentWrapTopRight">
                        <ul>
                            <li>
                                <Link to="#">
                                    <i className="fa fa-cube"></i>
                                    <span>Dashboard</span>
                                </Link>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>
        <section className="contentWrap">
            <div className="row">
                <div className="col-xs-12">
                    <div className="typography text-right">
                        <h2 id="mtTime"></h2>
                    </div>
                </div>
            </div>
            <div className="row">
                <div className="col-xs-12 col-sm-6">
                    <section className="boxWrap">
                        <div className="box">
                            <div className="boxHeader">
                                <div className="boxHeaderLeft">
                                    <h3>
                                        <i className="fa fa-th"></i>
                                        <span>Charts</span>
                                    </h3>
                                </div>
                                <div className="boxHeaderRight">

                                </div>
                            </div>
                            <div className="boxBody">
                                <BarChart data={chartData} options={chartOptions}/>
                            </div>
                        </div>
                    </section>
                </div>
                <div className="col-xs-12 col-sm-6">
                    <section className="boxWrap">
                        <div className="box">
                            <div className="boxHeader">
                                <div className="boxHeaderLeft">
                                    <h3>
                                        <i className="fa fa-signal"></i>
                                        <span>Beacons</span>
                                    </h3>
                                </div>
                                <div className="boxHeaderRight">

                                </div>
                            </div>
                            <div className="boxBody">
                                <div className="tableWrap">
                                    <table className="table table-bordered">
                                        <thead>
                                        <tr>
                                            <th>OS</th>
                                            <th>MAC address</th>
                                            <th>Zone</th>
                                            <th>Beacon</th>
                                            <th>Proximity</th>
                                            <th>Date</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>iOS</td>
                                            <td>00:00:00:00:00:0X</td>
                                            <td>Kitchen</td>
                                            <td>Beacon ID</td>
                                            <td>15 Meters</td>
                                            <td>21/Jan/2017</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
            <div className="row">
                <div className="col-xs-12 col-sm-6">
                    <section className="boxWrap">
                        <div className="box">
                            <div className="boxHeader">
                                <div className="boxHeaderLeft">
                                    <h3>
                                        <i className="fa fa-th"></i>
                                        <span>Stats</span>
                                    </h3>
                                </div>
                                <div className="boxHeaderRight">

                                </div>
                            </div>
                            <div className="boxBody">
                                <div className="tableWrap">
                                    <table className="table table-bordered">
                                        <thead>
                                        <tr>
                                            <th>In</th>
                                            <th>Out</th>
                                            <th>Date</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        {people.map(function(object, i){
                                            return <tr key={i}>
                                                {[
                                                    <td>{object.in}</td>,
                                                    <td>{object.out}</td>,
                                                    <td>{object.date}</td>
                                                ]}
                                            </tr>;
                                        })}
                                        <Websocket url={wsUrl}
                                                   onMessage={handleData}/>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </section>
      </div>
);

Dashboard.propTypes = {
  secretData: PropTypes.string.isRequired,
    handleData: PropTypes.func.isRequired,
    wsUrl: PropTypes.string.isRequired,
    chartData: PropTypes.any.isRequired,
    chartOptions: PropTypes.any.isRequired
};

export default Dashboard;
