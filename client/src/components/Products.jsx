import React, { PropTypes } from 'react';

const Products = ({products}) => (
  <div>
      <section className="contentWrapTop">
          <div className="row">
              <div className="col-xs-12">
                  <div className="contentWrapTopLeft">
                      <h1>Products</h1>
                  </div>
              </div>
          </div>
      </section>
      <section className="contentWrap">
          <div className="row">
              <div className="col-xs-12">
                  <div className="typography text-right">
                      <h2 id="mtTime"></h2>
                  </div>
              </div>
          </div>
          <div className="row">
              <div className="col-xs-12">
                  <section className="boxWrap">
                      <div className="box">
                          <div className="boxHeader">
                              <div className="boxHeaderLeft">
                                  <h3>
                                      <i className="fa fa-shopping-bag"></i>
                                      <span>Products</span>
                                  </h3>
                              </div>
                              <div className="boxHeaderRight">

                              </div>
                          </div>
                          <div className="boxBody">
                              <div className="tableWrap">
                                  <table className="table table-bordered table-valignM">
                                      <thead>
                                      <tr>
                                          <th>Product ID</th>
                                          <th>Description</th>
                                          <th>Image</th>
                                          <th>Price</th>
                                          <th>Date Registered</th>
                                          <th>Status</th>
                                      </tr>
                                      </thead>
                                      <tbody>

                                      {products.map(function(object, i){
                                          return <tr key={i}>
                                              {[
                                                  <td>{object.id}</td>,
                                                  <td>{object.description}</td>, // remove the key
                                                  <td><img src={object.image} alt="" height="50"/></td>,
                                                  <td>{object.price}</td>,
                                                  <td>{object.creation_date}</td>,
                                                  <td>{ object.state ===1? <span className="label label-success">Available</span>:
                                                  [ object.state ===2?<span className="label label-warning">Pending</span>:<span className="label label-danger">Expired</span>]}</td>
                                              ]}
                                          </tr>;
                                      })}
                                      
                                      </tbody>
                                  </table>
                              </div>
                          </div>
                      </div>
                  </section>
              </div>
          </div>
      </section>

  </div>
);




Products.propTypes = {
    products: PropTypes.array.isRequired
};


export default Products;
