import React, { PropTypes } from 'react';

const Users = ({users}) => (
    <div>
        <section className="contentWrapTop">
            <div className="row">
                <div className="col-xs-12">
                    <div className="contentWrapTopLeft">
                        <h1>Users</h1>
                    </div>
                </div>
            </div>
        </section>
        <section className="contentWrap">
            <div className="row">
                <div className="col-xs-12">
                    <div className="typography text-right">
                        <h2 id="mtTime"></h2>
                    </div>
                </div>
            </div>
            <div className="row">
                <div className="col-xs-12">
                    <section className="boxWrap">
                        <div className="box">
                            <div className="boxHeader">
                                <div className="boxHeaderLeft">
                                    <h3>
                                        <i className="fa fa-users"></i>
                                        <span>Users</span>
                                    </h3>
                                </div>
                                <div className="boxHeaderRight">

                                </div>
                            </div>
                            <div className="boxBody">
                                <div className="tableWrap">
                                    <table className="table table-bordered table-valignM">
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Username</th>
                                            <th>Email</th>
                                            <th>Type</th>
                                            <th>Date Registered</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        {users.map(function(object, i){
                                            return <tr  key={i}>
                                                {[
                                                    <td>{object.id}</td>,
                                                    <td>{object.username}</td>,
                                                    <td>{object.email}</td>,
                                                    <td>{object.type}</td>,
                                                    <td>{object.creation_date}</td>
                                                ]}
                                            </tr>;
                                        })}
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </section>
    </div>
);

Users.propTypes = {
    users: PropTypes.array.isRequired
};


export default Users;
