import Base from './components/Base.jsx';
import HomePage from './components/HomePage.jsx';
import DashboardPage from './containers/DashboardPage.jsx';
import ProductsPage from './containers/ProductsPage.jsx';
import PaymentsPage from './containers/PaymentsPage.jsx';
import UsersPage from './containers/UsersPage.jsx';
import LoginPage from './containers/LoginPage.jsx';
import ActivityLogPage from './containers/ActivityLogPage.jsx';
import Auth from './modules/Auth';


const routes = {
  // base component (wrapper for the whole application).
  component: Base,
  childRoutes: [

    {
      path: '/',
      getComponent: (location, callback) => {
        if (Auth.isUserAuthenticated()) {
          callback(null, DashboardPage);
        } else {
          callback(null, LoginPage);
        }
      }
    },

    {
      path: '/login',
      component: LoginPage
    },

    // {
    //   path: '/signup',
    //   component: SignUpPage
    // },

    {
      path: '/logout',
      onEnter: (nextState, replace) => {
        Auth.deauthenticateUser();

        // change the current URL to /
        replace('/');
      }
    },

    {
      path: '/products',
      getComponent: (location, callback) => {
        if (Auth.isUserAuthenticated()) {
          callback(null, ProductsPage);
        } else {
          callback(null, LoginPage);
        }
      }
    },

      {
          path: '/payments',
          getComponent: (location, callback) => {
              if (Auth.isUserAuthenticated()) {
                  callback(null, PaymentsPage);
              } else {
                  callback(null, LoginPage);
              }
          }

      } ,
      {
          path: '/users',
          getComponent: (location, callback) => {
              if (Auth.isUserAuthenticated()) {
                  callback(null, UsersPage);
              } else {
                  callback(null, LoginPage);
              }
          }
      },
      {
          path: '/activity_log',
          getComponent: (location, callback) => {
              if (Auth.isUserAuthenticated()) {
                  callback(null, ActivityLogPage);
              } else {
                  callback(null, LoginPage);
              }
          }
      }
  ]
};

export default routes;
