'use strict';
const express = require('express');
const bodyParser = require('body-parser');
const passport = require('passport');
const config = require('./config');
const knex = require('./server/models/index');
const dgram = require('dgram');
const cors = require('cors');
global.path = require('path');
global.CONFIG = {};
global.db = knex;

class App {
    constructor(){
        this.app = express();
        // tell the app to look for static files in these directories
        // app.use(express.static('./server/static/'));
        // app.use(express.static('./client/dist/'));
        this.app.use(cors());
        // tell the app to parse HTTP body messages
        this.app.use(bodyParser.urlencoded({ extended: false }));
        // pass the passport middleware
        this.app.use(passport.initialize());

        // load passport strategies
        const localSignupStrategy = require('./server/passport/local-signup');
        const localLoginStrategy = require('./server/passport/local-login');
        passport.use('local-signup', localSignupStrategy);
        passport.use('local-login', localLoginStrategy);

        // routes
        const authRoutes = require('./server/routes/auth');
        const apiRoutes = require('./server/routes/api');
        this.app.use('/auth', authRoutes);
        this.app.use('/api', apiRoutes);

        // app.get('*', (req, res) => {
        //     //res.sendFile(express.static('./server/static/'));
        //     res.sendFile(path.resolve(__dirname, 'server', 'static', 'index.html'));
        //     //  res.send(__dirname);
        // });

        // websocket and http servers
        this.webSocketServer = require('websocket').server;

        this.webSocketsServerPort = 1337;

        this.http = require('http');

        this.server = this.http.createServer(function(request, response) {
            // Not important for us. We're writing WebSocket server, not HTTP server
        });
        /**
         * WebSocket server
         */
        this.wsServer = new this.webSocketServer({
            // WebSocket server is tied to a HTTP server. WebSocket request is just
            // an enhanced HTTP request. For more info http://tools.ietf.org/html/rfc6455#page-6
            httpServer: this.server
        });
    }

    Start() {
        // start the server
        this.app.listen(3000, () => {
            console.log('Api server started (TCP:3000)');
        });
    }

    StartApc() {

        // Port where we'll run the websocket server
        let webSocketsServerPort = this.webSocketsServerPort;

        let clients = [ ];
        let oldMessages = [ ];
        let num = 0;

        this.server.listen(webSocketsServerPort, function() {
            console.log("websocket server started (TCP:1337)");
        });

        // This callback function is called every time someone
        // tries to connect to the WebSocket server
        this.wsServer.on('request', function(request) {
            // console.log((new Date()) + ' Connection from origin ' + request.origin + '.');

            // accept connection - you should check 'request.origin' to make sure that
            // client is connecting from your website
            // (http://en.wikipedia.org/wiki/Same_origin_policy)
            let connection = request.accept(null, request.origin);
            // we need to know client index to remove them on 'close' event

            let index = clients.push(connection) - 1;

            // console.log((new Date()) + ' Connection accepted.');

            // send back chat history
            // if (this.history.length > 0) {
//                connection.sendUTF(JSON.stringify( { type: 'history', data: history} ));
//             }
//             num++;
//             for (var i=0; i < clients.length; i++) {
//                 clients[i].sendUTF(JSON.stringify({'in': num, 'out': 1}));
//             }
            let sendData = {};
            sendData.first = true;
            sendData.List = oldMessages;
            connection.sendUTF(JSON.stringify(sendData));
             // console.log("client connected");
            // user sent some message
            connection.on('message', function(message) {
                if (message.type === 'utf8') { // accept only text
                    // if (userName === false) { // first message sent by user is their name
                    //     // remember user name
                    //     userName = htmlEntities(message.utf8Data);
                    //     // get random color and send it back to the user
                    //     userColor = colors.shift();
                    //     connection.sendUTF(JSON.stringify({ type:'color', data: userColor }));
                    //     console.log((new Date()) + ' User is known as: ' + userName
                    //         + ' with ' + userColor + ' color.');
                    //
                    // } else { // log and broadcast the message
                    //     console.log((new Date()) + ' Received Message from '
                    //         + userName + ': ' + message.utf8Data);
                    //
                    //     // we want to keep history of all sent messages
                    //     var obj = {
                    //         time: (new Date()).getTime(),
                    //         text: htmlEntities(message.utf8Data),
                    //         author: userName,
                    //         color: userColor
                    //     };
                    //     history.push(obj);
                    //     history = history.slice(-100);
                    //
                    //     // broadcast message to all connected clients
                    //     var json = JSON.stringify({ type:'message', data: obj });
                    //     for (var i=0; i < clients.length; i++) {
                    //         clients[i].sendUTF(json);
                    //     }
                    // }
                }
            });

            // user disconnected
            connection.on('close', function(connection) {
                // if (userName !== false && userColor !== false) {
                //     console.log((new Date()) + " Peer "
                //         + connection.remoteAddress + " disconnected.");
                //     // remove user from the list of connected clients
                //     // push back user's color to be reused by another user
                //     colors.push(userColor);
                // }
                // console.log("client disconnected");
                clients.splice(index, 1);
            });

        });

        const server = dgram.createSocket('udp4');

        server.on('error', (err) => {
            console.log(`server error:\n${err.stack}`);
            server.close();
        });

        server.on('message', (msg, rinfo) => {
            var massage = JSON.parse(msg);

            let date = new Date();
            massage.date = date.toString();

            oldMessages.splice(0, 0, massage);
            if (oldMessages.length > 3) {
                oldMessages.pop();
            }

            let sendData = {};
            sendData.first = false;
            sendData.List = massage;

            // console.log(`server got: ${msg} from ${rinfo.address}:${rinfo.port}`);
            // console.log(massage);
            for (var i=0; i < clients.length; i++) {
                clients[i].sendUTF(JSON.stringify(sendData));
            }
        });

        var message = new Buffer('My KungFu is Good!');

        server.send(message, 0, message.length, 7777, '127.0.0.1', function(err, bytes) {
            if (err) throw err;
            console.log('connected to counter (UDP:7777)');
        });
    }


    StartWebSocket() {
    }
}

module.exports = new App();