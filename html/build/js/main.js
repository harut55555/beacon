$(document).ready(function() {
	$(".selectize").each(function () {
		$(this).selectize();
	});

	$(".selectize-tags").each(function () {
		$(this).selectize({
			plugins: ["remove_button", "drag_drop"],
			delimiter: ",",
			persist: false,
			create: function (input) {
				return {
					value: input,
					text: input
				}
			}
		});
	});

	var sideMenuToggle = $(".sideMenuToggle");

	sideMenuToggle.click(function (e) {
		e.preventDefault();
		var body = $("body");
		if (body.hasClass("mtSidebarOpen")) {
			var sidebarCollapsedLink = $(".mtSidebar a[data-toggle='collapse']:not(.collapsed)");
			sidebarCollapsedLink.addClass("collapsed");
			var sidebarCollapsedPanel = $(".mtSidebar .panel-collapse.in");
			sidebarCollapsedPanel.removeClass("in");
		}
		body.toggleClass("mtSidebarOpen");
	});

	var sidebarHeadingCollapseLink = $(".mtSidebar .panel-heading a[data-toggle='collapse']");

	sidebarHeadingCollapseLink.click(function () {
		if (!$("body").hasClass("mtSidebarOpen")) {
			return false;
		}
	});

	function startTime() {
		var today = new Date();
		var h = today.getHours();
		var m = today.getMinutes();
		var s = today.getSeconds();
		m = checkTime(m);
		s = checkTime(s);
		document.getElementById("mtTime").innerHTML =
		 h + ":" + m + ":" + s;
		var t = setTimeout(startTime, 500);
	}
	function checkTime(i) {
		if (i < 10) {i = "0" + i}
		return i;
	}
	startTime();
});