var knex = require('knex')({
    client: 'mysql',
    connection: {
        host : CONFIG.db.host,
        user : CONFIG.db.user,
        password : CONFIG.db.password,
        database : CONFIG.db.database
    }
});

module.exports = knex;