const express = require('express');
const products = require('../models/product');
const payments = require('../models/payment');
const activities = require('../models/activity');
const users = require('../models/User');

const router = new express.Router();

router.get('/dashboard', (req, res) => {
  res.setHeader('Access-Control-Allow-Origin', 'http://127.0.0.1:8888');
  res.status(200).json({
    message: "You're authorized to see this secret message."
  });
});

router.get('/get_products', (req, res) => {

      products.getList((error, list) => {

        //res.setHeader('Access-Control-Allow-Origin', 'http://127.0.0.1:8888');
        res.status(200).json({
          status: !error,
            list: list
        });
     });
});

router.get('/get_payments', (req, res) => {

    payments.getList((error, list) => {

        //res.setHeader('Access-Control-Allow-Origin', 'http://127.0.0.1:8888');
        res.status(200).json({
            status: !error,
            list: list
        });
    });
});

router.get('/get_activities', (req, res) => {

    activities.getList((error, list) => {

        //res.setHeader('Access-Control-Allow-Origin', 'http://127.0.0.1:8888');
        res.status(200).json({
            status: !error,
            list: list
        });
    });
});

router.get('/get_users', (req, res) => {

    users.getList((error, list) => {

        //res.setHeader('Access-Control-Allow-Origin', 'http://127.0.0.1:8888');
        res.status(200).json({
            status: !error,
            list: list
        });
    });
});
module.exports = router;
